export enum GameStatus {
    PLAYER_X_TURN = 'PLAYER_X_TURN',
    PLAYER_O_TURN = 'PLAYER_O_TURN',
    PLAYER_X_WIN = 'PLAYER_X_WIN',
    PLAYER_O_WIN = 'PLAYER_O_WIN',
    TIE = 'TIE'
}