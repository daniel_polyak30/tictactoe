import {Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Cell} from "./enums/Cell";
import {GameStatus} from "./enums/GameStatus";
import {Player} from "./enums/Player";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: []
})
export class AppComponent implements OnInit {
    title = 'Tic Tac Toe';
    cellType = Cell;
    gameStatus = GameStatus;
    gameId: any;
    actualGameStatus: GameStatus | undefined;
    actualPlayer: any;
    winner: any;
    isTie: any;

    topLeftCellValue: string = "";
    topCenterCellValue: string = "";
    topRightCellValue: string = "";
    middleLeftCellValue: string = "";
    middleCenterCellValue: string = "";
    middleRightCellValue: string = "";
    bottomLeftCellValue: string = "";
    bottomCenterCellValue: string = "";
    bottomRightCellValue: string = "";

    playerXScore: number = 0;
    playerOScore: number = 0;
    tieScore: number = 0;

    constructor(private http: HttpClient) {
    }

    ngOnInit() {
        this.getScores();
    }

    public createNewGame(): void {
        this.reloadBoard();
        this.http.post<any>('api/game/create', null).subscribe(data => {
            if (data && data.gameId && data.gameStatus) {
                this.gameId = data.gameId;
                this.actualGameStatus = data.gameStatus;
                this.handleNewGameStatus();
            }
        });
    }

    public isGameFinished(): boolean {
        if (!this.actualGameStatus) {
            return false;
        }

        if (this.actualGameStatus != this.gameStatus.PLAYER_X_TURN && this.actualGameStatus != this.gameStatus.PLAYER_O_TURN) {
            return false;
        }
        return true;
    }

    public createMove(cell: Cell): void {
        if (!this.gameId && !this.actualGameStatus) {
            return;
        }

        let player: Player = this.getPlayerByStatus();
        if (!player) {
            return;
        }

        let moveRequest: any = {
            gameId: this.gameId,
            player: player,
            cell: cell
        };

        this.http.post<any>('api/move/', moveRequest).subscribe(data => {
            if (data && data.moveResponse) {
                if (data.moveResponse.availableMove) {
                    if (player == Player.PLAYER_X) {
                        this.setCellValue('X', cell);
                    } else if (player == Player.PLAYER_O) {
                        this.setCellValue('O', cell);
                    }
                    this.actualGameStatus = data.moveResponse.newGameStatus;
                    this.handleNewGameStatus();
                }
            }
        });
    }

    private setCellValue(mark: string, cell: Cell): void {
        switch (cell) {
            case this.cellType.TOP_LEFT:
                this.topLeftCellValue = mark;
                break;
            case this.cellType.TOP_CENTER:
                this.topCenterCellValue = mark;
                break;
            case this.cellType.TOP_RIGHT:
                this.topRightCellValue = mark;
                break;
            case this.cellType.MIDDLE_LEFT:
                this.middleLeftCellValue = mark;
                break;
            case this.cellType.MIDDLE_CENTER:
                this.middleCenterCellValue = mark;
                break;
            case this.cellType.MIDDLE_RIGHT:
                this.middleRightCellValue = mark;
                break;
            case this.cellType.BOTTOM_LEFT:
                this.bottomLeftCellValue = mark;
                break;
            case this.cellType.BOTTOM_CENTER:
                this.bottomCenterCellValue = mark;
                break;
            case this.cellType.BOTTOM_RIGHT:
                this.bottomRightCellValue = mark;
                break;
        }
    }

    private handleNewGameStatus(): void {
        switch (this.actualGameStatus) {
            case this.gameStatus.PLAYER_X_TURN:
                this.actualPlayer = 'X';
                if (this.winner) {
                    this.winner = null;
                }
                if (this.isTie) {
                    this.isTie = false;
                }
                break;
            case this.gameStatus.PLAYER_O_TURN:
                this.actualPlayer = 'O';
                if (this.winner) {
                    this.winner = null;
                }
                if (this.isTie) {
                    this.isTie = false;
                }
                break;
            case this.gameStatus.PLAYER_X_WIN:
                this.winner = 'X';
                if (this.actualPlayer) {
                    this.actualPlayer = null;
                }
                if (this.isTie) {
                    this.isTie = false;
                }

                this.getScores();

                break;
            case this.gameStatus.PLAYER_O_WIN:
                this.winner = 'O';
                if (this.actualPlayer) {
                    this.actualPlayer = null;
                }
                if (this.isTie) {
                    this.isTie = false;
                }

                this.getScores();

                break;
            case this.gameStatus.TIE:
                if (this.actualPlayer) {
                    this.actualPlayer = null;
                }
                if (this.winner) {
                    this.winner = null;
                }
                this.isTie = true;

                this.getScores();

                break;
        }
    }

    private getPlayerByStatus(): Player | any {
        if (this.actualGameStatus == this.gameStatus.PLAYER_X_TURN) {
            return Player.PLAYER_X;
        }

        if (this.actualGameStatus == this.gameStatus.PLAYER_O_TURN) {
            return Player.PLAYER_O;
        }

        return null;
    }

    private reloadBoard(): void {
        this.topLeftCellValue = "";
        this.topCenterCellValue = "";
        this.topRightCellValue = "";
        this.middleLeftCellValue = "";
        this.middleCenterCellValue = "";
        this.middleRightCellValue = "";
        this.bottomLeftCellValue = "";
        this.bottomCenterCellValue = "";
        this.bottomRightCellValue = "";

        this.actualPlayer = null;
        this.winner = null;
        this.isTie = false;
    }

    private getScores(): void {
        this.http.get<any>('api/game/scores').subscribe(data => {
            if (data && data.score) {
                if (data.score.playerX) {
                    this.playerXScore = data.score.playerX;
                }
                if (data.score.playerO) {
                    this.playerOScore = data.score.playerO;
                }
                if (data.score.tie) {
                    this.tieScore = data.score.tie;
                }
            }
        });
    }
}
