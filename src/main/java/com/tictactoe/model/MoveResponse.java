package com.tictactoe.model;

import com.tictactoe.enums.GameStatus;

public class MoveResponse {

    private boolean availableMove;
    private GameStatus newGameStatus;

    public boolean isAvailableMove() {
        return availableMove;
    }

    public void setAvailableMove(boolean availableMove) {
        this.availableMove = availableMove;
    }

    public GameStatus getNewGameStatus() {
        return newGameStatus;
    }

    public void setNewGameStatus(GameStatus newGameStatus) {
        this.newGameStatus = newGameStatus;
    }
}
