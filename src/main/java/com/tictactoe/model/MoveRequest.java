package com.tictactoe.model;

import com.tictactoe.enums.Cell;
import com.tictactoe.enums.Player;

public class MoveRequest {

    private String gameId;
    private Player player;
    private Cell cell;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }
}
