package com.tictactoe.model;

public class Score {

    private int playerX;
    private int playerO;
    private int tie;

    public Score (int playerX, int playerO, int tie) {
        this.playerX = playerX;
        this.playerO = playerO;
        this.tie = tie;
    }

    public int getPlayerX() {
        return playerX;
    }

    public void setPlayerX(int playerX) {
        this.playerX = playerX;
    }

    public int getPlayerO() {
        return playerO;
    }

    public void setPlayerO(int playerO) {
        this.playerO = playerO;
    }

    public int getTie() {
        return tie;
    }

    public void setTie(int tie) {
        this.tie = tie;
    }
}
