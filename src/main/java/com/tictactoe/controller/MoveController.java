package com.tictactoe.controller;

import com.tictactoe.model.MoveRequest;
import com.tictactoe.model.MoveResponse;
import com.tictactoe.service.TictactoeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/move")
@CrossOrigin(origins = "http://localhost:4200")
public class MoveController {

    @Autowired
    private TictactoeService tictactoeService;

    /**
     * Kapott MoveRequest objektum alapján megnézi,
     * hogy a lépés szabályos-e és azt, hogy ezzel a lépéssel
     * nyert-e valamelyik játékos vagy hogy döntetlen lett-e a játék.
     * @param moveRequest
     * @return
     */
    @PostMapping("/")
    public ModelMap handleMove(@RequestBody MoveRequest moveRequest) {
        MoveResponse moveResponse = tictactoeService.handleMove(moveRequest);
        return new ModelMap("moveResponse", moveResponse);
    }
}
