package com.tictactoe.controller;

import com.tictactoe.model.Game;
import com.tictactoe.model.Score;
import com.tictactoe.service.TictactoeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/game")
@CrossOrigin(origins = "http://localhost:4200")
public class GameController {

    @Autowired
    private TictactoeService tictactoeService;

    /**
     * Létrehoz egy új Game objektumot és elmenti adatbázisba.
     * @return
     */
    @PostMapping("/create")
    public ModelMap createNewGame() {
        Game game = tictactoeService.createNewGame();

        return new ModelMap("gameId", game.getId()).addAttribute("gameStatus", game.getStatus());
    }

    /**
     * Lekérdezi az összes befejezett játékot és összeszámolja,
     * hogy melyik játékos hányszor nyert és hogy mennyi döntetlen játék volt.
     * @return
     */
    @GetMapping("/scores")
    public ModelMap getScores() {
        Score score = tictactoeService.getScores();

        return new ModelMap("score", score);
    }
}
