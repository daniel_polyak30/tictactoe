package com.tictactoe.repository;

import com.tictactoe.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GameRepository extends JpaRepository<Game, String> {
    @Query(
            value = "SELECT * FROM games g WHERE g.status IN ('PLAYER_X_WIN', 'PLAYER_O_WIN', 'TIE')",
            nativeQuery = true)
    List<Game> findAllFinishedGame();
}
