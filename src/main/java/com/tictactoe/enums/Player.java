package com.tictactoe.enums;

public enum Player {
    PLAYER_X,
    PLAYER_O;
}
