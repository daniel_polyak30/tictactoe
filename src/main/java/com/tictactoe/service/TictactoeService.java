package com.tictactoe.service;

import com.tictactoe.enums.Cell;
import com.tictactoe.enums.GameStatus;
import com.tictactoe.enums.Player;
import com.tictactoe.model.*;
import com.tictactoe.repository.GameRepository;
import com.tictactoe.repository.MoveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.tictactoe.enums.GameStatus.*;

@Service
public class TictactoeService {

    private static final int MAX_MOVE_SIZE = 9;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private MoveRepository moveRepository;

    /**
     * Lekérdezi kapott ID alapján a Game objektumot.
     * @param id
     * @return
     */
    public Game getGameById(String id) {
        return gameRepository.findById(id).orElse(null);
    }

    /**
     * Létrehoz egy új játékot és ha volt előző játék, akkor az alapján beállítja a játék státuszát
     * @return
     */
    public Game createNewGame() {
        Game game = new Game();
        game.setCreateDate(new Date());

        Game lastGame = getLastGame();
        if (lastGame == null) {
            game.setStatus(PLAYER_X_TURN);

            saveGame(game);

            return game;
        }

        setGameStatusByLastGame(game, lastGame);
        saveGame(game);

        return game;
    }

    /**
     * Előző játék alapján beállítja a játék státuszát
     * @param game
     * @param lastGame
     */
    private void setGameStatusByLastGame(Game game, Game lastGame) {
        switch (lastGame.getStatus()) {
            case PLAYER_X_TURN:
            case PLAYER_O_TURN:
            case PLAYER_O_WIN:
            case TIE:
                game.setStatus(PLAYER_X_TURN);
                break;
            case PLAYER_X_WIN:
                game.setStatus(PLAYER_O_TURN);
                break;
        }
    }

    /**
     * Elmenti a Game objektumot
     * @param game
     */
    private void saveGame(Game game) {
        gameRepository.saveAndFlush(game);
    }

    /**
     * Lekérdezi az utolsó játék a createDate mező alapján
     * @return
     */
    private Game getLastGame() {
        Sort sort = Sort.by(Sort.Direction.DESC, "createDate");
        List<Game> games = gameRepository.findAll(sort);
        if (games.isEmpty()) {
            return null;
        }

        return games.get(0);
    }

    /**
     * Kapott MoveRequest objektum alapján megnézi,
     * hogy a lépés szabályos-e és azt, hogy ezzel a lépéssel
     * nyert-e valamelyik játékos vagy hogy döntetlen lett-e a játék.
     * @param moveRequest
     * @return
     */
    public MoveResponse handleMove(MoveRequest moveRequest) {
        String gameId = moveRequest.getGameId();
        Game game = gameRepository.getById(gameId);
        if (isGameFinished(game.getStatus())) {
            return new MoveResponse();
        }

        if ((PLAYER_X_TURN.equals(game.getStatus()) && Player.PLAYER_O.equals(moveRequest.getPlayer()))
                || (PLAYER_O_TURN.equals(game.getStatus()) && Player.PLAYER_X.equals(moveRequest.getPlayer()))) {

            return new MoveResponse();
        }

        List<Move> usedMoves = moveRepository.findAllByGameId(gameId);
        boolean isMoveAvailable = usedMoves.stream().noneMatch(m -> moveRequest.getCell().equals(m.getCell()));
        if (!isMoveAvailable) {
            return new MoveResponse();
        }

        MoveResponse moveResponse = new MoveResponse();
        moveResponse.setAvailableMove(true);

        Move newMove = createMove(moveRequest, game);
        usedMoves.add(newMove);

        List<Cell> usedCellsByPlayer = usedMoves.stream()
                .filter(m -> moveRequest.getPlayer().equals(m.getPlayer()))
                .map(Move::getCell)
                .collect(Collectors.toList());
        List<List<Cell>> winningPositions = getWinningPositions();
        boolean isWinner = winningPositions.stream().anyMatch(usedCellsByPlayer::containsAll);
        GameStatus newGameStatus = null;
        if (isWinner) {
            if (Player.PLAYER_X.equals(moveRequest.getPlayer())) {
                newGameStatus = PLAYER_X_WIN;
            } else {
                newGameStatus = PLAYER_O_WIN;
            }
        } else {
            if (MAX_MOVE_SIZE == usedMoves.size()) {
                newGameStatus = TIE;
            } else {
                if (Player.PLAYER_X.equals(moveRequest.getPlayer())) {
                    newGameStatus = PLAYER_O_TURN;
                } else {
                    newGameStatus = PLAYER_X_TURN;
                }
            }
        }

        game.setStatus(newGameStatus);
        saveGame(game);

        moveResponse.setNewGameStatus(newGameStatus);

        return moveResponse;
    }

    /**
     * Megnézi, hogy a játék befejeződött-e már
     * @param status
     * @return
     */
    private boolean isGameFinished(GameStatus status) {
        return !PLAYER_X_TURN.equals(status) && !PLAYER_O_TURN.equals(status);
    }

    /**
     * Létrehoz egy Move objektumot és elmenti a kapott MoveRequest objektum alapján
     * @param moveRequest
     * @param game
     * @return
     */
    private Move createMove(MoveRequest moveRequest, Game game) {
        Move move = new Move();
        move.setCell(moveRequest.getCell());
        move.setPlayer(moveRequest.getPlayer());
        move.setGame(game);

        moveRepository.saveAndFlush(move);

        return move;
    }

    /**
     * Egy listában visszaadja a lehetséges győztes pozicíókat.
     * @return
     */
    private List<List<Cell>> getWinningPositions() {
        List<List<Cell>> winningPositions = new ArrayList<>();

        winningPositions.add(Arrays.asList(Cell.TOP_LEFT, Cell.TOP_CENTER, Cell.TOP_RIGHT));
        winningPositions.add(Arrays.asList(Cell.MIDDLE_LEFT, Cell.MIDDLE_CENTER, Cell.MIDDLE_RIGHT));
        winningPositions.add(Arrays.asList(Cell.BOTTOM_LEFT, Cell.BOTTOM_CENTER, Cell.BOTTOM_RIGHT));

        winningPositions.add(Arrays.asList(Cell.TOP_LEFT, Cell.MIDDLE_LEFT, Cell.BOTTOM_LEFT));
        winningPositions.add(Arrays.asList(Cell.TOP_CENTER, Cell.MIDDLE_CENTER, Cell.BOTTOM_CENTER));
        winningPositions.add(Arrays.asList(Cell.TOP_RIGHT, Cell.MIDDLE_RIGHT, Cell.BOTTOM_RIGHT));

        winningPositions.add(Arrays.asList(Cell.TOP_LEFT, Cell.MIDDLE_CENTER, Cell.BOTTOM_RIGHT));
        winningPositions.add(Arrays.asList(Cell.BOTTOM_LEFT, Cell.MIDDLE_CENTER, Cell.TOP_RIGHT));

        return winningPositions;
    }

    /**
     * Lekéri az összes befejezett játékot és az alapján, hogy ki nyert feltölti a pontszámokat.
     * @return
     */
    public Score getScores() {
        List<Game> finishedGames = gameRepository.findAllFinishedGame();

        int playerX = 0;
        int playerO = 0;
        int tie = 0;

        for (Game game : finishedGames) {
            switch (game.getStatus()) {
                case PLAYER_X_WIN:
                    playerX++;
                    break;
                case PLAYER_O_WIN:
                    playerO++;
                    break;
                case TIE:
                    tie++;
                    break;
            }
        }

        return new Score(playerX, playerO, tie);
    }
}
